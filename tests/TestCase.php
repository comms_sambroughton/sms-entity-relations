<?php

namespace Tests;

use App\Entities\BaseEntity;
use App\Models\BaseModel;
use App\Repositories\BaseRepository;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Mockery;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();
        app()->bind(TestRepositoryContract::class, TestRepository::class);
    }
//
//    protected function makeTestTables(...$tables)
//    {
//        collect($tables)->each(function ($data) {
//            $this->app['db']->connection()->getSchemaBuilder()->create($data['name'], function (Blueprint $table) use ($data) {
//                $table->increments('id');
////                $table->string('name');
////                $table->string('relation_type')->nullable();
////                $table->integer('foo_id')->nullable();
//                foreach ($data['cols'] as $col) {
//                    $table->{$col['type']}($col['name']);
//                }
//                $table->timestamps();
//            });
//        });
//    }

    protected function makeTestTables(array $tables)
    {
        foreach ($tables as $name => $data) {
            $this->app['db']->connection()->getSchemaBuilder()->create($name, function (Blueprint $table) use ($data) {
                $table->increments('id');
                foreach ($data as $name => $type) {
                    $table->{$type}($name);
                }
                $table->timestamps();
            });
        }
    }

    protected function makeRepo($model): BaseRepository
    {
        return new class($model) extends BaseRepository{};
    }

    protected function getMockForBaseEntity($mockName, $repository, array $mockedMethods)
    {
        $mock = $this->getMockForAbstractClass(
            BaseEntity::class,
            [$repository],
            $mockName,
            true, true, true,
            $mockedMethods);


        app()->when($mockName)
            ->needs(BaseRepository::class)
            ->give(function () use ($repository) {
                return $repository;
            });

        return $mock;
    }

    protected function createEntity($entityTemplate, array $attributes, array $methods, $times = 1)
    {
        $entities = [];

        foreach (range(1, $times) as $i) {
            $entity = $entityTemplate->create($attributes);
            foreach ($methods as $methodName => $callback) {
                $entity->expects($this->any())
                    ->method($methodName)
                    ->will($this->returnCallback(function () use ($entity, $callback) {
                        return $callback($entity);
                    }));
            }
            $entities[] = $entity;
        };

        return count($entities) > 1 ? $entities : $entities[0];
    }

    protected function makeModel($number = 2)
    {
        $fooModel = new class extends BaseModel
        {
            protected $table = 'foos';
            protected $guarded = [];
        };

        $barModel = new class extends BaseModel
        {
            protected $table = 'bars';
            protected $guarded = [];
        };

        $bazModel = new class extends BaseModel
        {
            protected $table = 'bazs';
            protected $guarded = [];
        };

        return  array_slice([$fooModel, $barModel, $bazModel], 0, $number);
    }
}
