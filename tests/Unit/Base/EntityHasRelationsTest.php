<?php

namespace Tests\Feature;

use Tests\TestCase;

class EntityHasRelationsTest extends TestCase
{
    protected $testService;
    protected $testRepo;
    protected $fooClass;
    protected $barClass;
    protected $bazClass;

    protected function setUp()
    {
        parent::setUp();

        list($fooModel, $barModel, $bazModel) = $this->makeModel(3);
        $this->fooClass = $this->getMockForBaseEntity('FooClass', $this->makeRepo($fooModel), ['getRelationA', 'getRelationB']);
        $this->barClass = $this->getMockForBaseEntity('BarClass', $this->makeRepo($barModel), ['getRelationA', 'getRelationB']);
        $this->bazClass = $this->getMockForBaseEntity('BazClass', $this->makeRepo($bazModel), ['getRelationA', 'getRelationB']);
    }

    /** @test */
    function it_can_have_a_one_to_one_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string', 'foo_id' => 'integer'],
        ]);

        $foo = $this->createEntity($this->fooClass,
            ['name' => 'Foo'],
            ['getRelationA' => function ($entity) {
                return $entity->hasOne('BarClass', 'foo_id');
            }]);

        $bar = $this->createEntity($this->barClass,
            ['name' => 'Bar', 'foo_id' => $foo->getId()],
            ['getRelationA' => function ($entity) {
                return $entity->belongsTo('FooClass', 'foo_id');
            }]);

        $this->assertEquals('Bar', $foo->getRelationA()->name);
        $this->assertEquals('Foo', $bar->getRelationA()->name);

    }

    /** @test */
    function it_can_have_a_polymorphic_one_to_one_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string', 'foo_id' => 'integer', 'foo_type' => 'string'],
        ]);

        $foo = $this->createEntity($this->fooClass,
            ['name' => 'Foo'],
            ['getRelationA' => function ($entity) {
                return $entity->morphOne('BarClass', 'foo_type', 'foo_id');
            }]);

        $bar = $this->createEntity($this->barClass,
            ['name' => 'Bar',
                'foo_id' => $foo->getId(),
                'foo_type' => $foo->getType()],
            ['getRelationA' => function ($entity) {
                return $entity->morphTo('foo_type', 'foo_id');
            }]);

        $this->assertEquals('Bar', $foo->getRelationA()->name);
        $this->assertEquals('Foo', $bar->getRelationA()->name);
    }

    /** @test */
    function it_can_have_a_one_to_many_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string', 'foo_id' => 'integer']
        ]);

        $foo = $this->createEntity($this->fooClass,
            ['name' => 'Foo'],
            ['getRelationA' => function ($entity) {
                return $entity->hasMany('BarClass', 'foo_id');
            }]);

        list($barA, $barB) = $this->createEntity($this->barClass,
            ['name' => 'Bar',
                'foo_id' => $foo->getId()],
            ['getRelationA' => function ($entity) {
                return $entity->belongsTo('FooClass', 'foo_id');
            }
            ], 2);

        $this->assertCount(2, $foo->getRelationA());
        $this->assertEquals('Foo', $barA->getRelationA()->name);
        $this->assertEquals('Foo', $barB->getRelationA()->name);
    }

    /** @test */
    function it_can_have_a_polymorphic_one_to_many_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string'],
            'bazs' => ['name' => 'string', 'relatable_id' => 'integer', 'relatable_type' => 'string']
        ]);

        $getOwnedCallback = function ($entity) {
            return $entity->morphMany('BazClass', 'relatable_type', 'relatable_id');
        };
        $getOwnerCallback = function ($entity) {
            return $entity->morphTo('relatable_type', 'relatable_id');
        };

        $foo = $this->createEntity($this->fooClass,
            ['name' => 'Foo'],
            ['getRelationA' => $getOwnedCallback]);

        $bar = $this->createEntity($this->barClass,
            ['name' => 'Bar'],
            ['getRelationA' => $getOwnedCallback]);

        $bazA = $this->createEntity($this->bazClass, [
            'name' => 'Baz',
            'relatable_id' => $foo->getId(),
            'relatable_type' => $foo->getType()],
            ['getRelationA' => $getOwnerCallback]);

        list($bazB, $bazC) = $this->createEntity($this->bazClass, [
            'name' => 'Baz',
            'relatable_id' => $bar->getId(),
            'relatable_type' => $bar->getType()],
            ['getRelationA' => $getOwnerCallback], 2);

        $this->assertCount(1, $foo->getRelationA());
        $this->assertCount(2, $bar->getRelationA());
        $this->assertEquals('Foo', $bazA->getRelationA()->name);
        $this->assertEquals('Bar', $bazB->getRelationA()->name);
        $this->assertEquals('Bar', $bazC->getRelationA()->name);
    }

    /** @test */
    function it_can_have_a_many_to_many_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string'],
            'bar_foo' => ['foo_id' => 'integer', 'bar_id' => 'integer'],
        ]);

        list($fooA, $fooB) = $this->createEntity($this->fooClass, [
            'name' => 'Foo'],
            ['getRelationA' => function ($entity) {
                return $entity->belongsToMany('BarClass');
            }], 2);

        $barA = $this->createEntity($this->barClass, [
            'name' => 'Bar'],
            ['getRelationA' => function ($entity) {
                return $entity->belongsToMany('FooClass');
            }]);

        $fooA->attachBelongsToMany($barA);
        $barA->attachBelongsToMany($fooB);

        $this->assertCount(1, $fooA->getRelationA());
        $this->assertCount(1, $fooB->getRelationA());
        $this->assertCount(2, $barA->getRelationA());
    }

    /** @test */
    function it_can_have_a_polymorphic_many_to_many_relationship()
    {
        $this->makeTestTables([
            'foos' => ['name' => 'string'],
            'bars' => ['name' => 'string'],
            'bazs' => ['name' => 'string'],
            'bazzables' => ['baz_id' => 'integer', 'bazzable_id' => 'integer', 'bazzable_type' => 'string'],
        ]);

        $foo = $this->createEntity($this->fooClass, [
            'name' => 'Foo'],
            ['getRelationA' => function ($entity) {
                return $entity->morphToMany('BazClass', 'bazzable');
            }]);
        $bar = $this->createEntity($this->barClass, [
            'name' => 'Bar'],
            ['getRelationA' => function ($entity) {
                return $entity->morphToMany('BazClass', 'bazzable');
            }]);
        $baz = $this->createEntity($this->bazClass, [
            'name' => 'Baz'],
            [
                'getRelationA' => function ($entity) {
                    return $entity->morphedByMany('FooClass', 'bazzable');
                },
                'getRelationB' => function ($entity) {
                    return $entity->morphedByMany('BarClass', 'bazzable');
                },
            ]);

        $foo->attachMorphToMany('bazzable', $baz);
        $baz->attachMorphedByMany('bazzable', $bar);

        $this->assertCount(1, $foo->getRelationA());
        $this->assertCount(1, $bar->getRelationA());
        $this->assertCount(1, $baz->getRelationA());
        $this->assertEquals('Foo', $baz->getRelationA()->first()->name);
        $this->assertCount(1, $baz->getRelationB());
        $this->assertEquals('Bar', $baz->getRelationB()->first()->name);
    }

}
