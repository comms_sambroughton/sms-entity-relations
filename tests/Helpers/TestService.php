<?php

namespace Tests;

use App\Entities\BaseEntity;
use App\Models\BaseModel;
use App\Repositories\BaseRepository;
use App\Services\BaseService;
use App\Traits\EntityHasRelations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Traits\Macroable;

class TestService extends BaseService
{
    public function __construct(TestEntity $entity)
    {
        parent::__construct($entity);
    }
}

class TestEntity extends BaseEntity
{
    use Macroable;
    protected $name;

    public function __construct(TestRepositoryContract $repo)
    {
        parent::__construct($repo);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}

class TestEntityHasRelations extends TestEntity
{
    use EntityHasRelations;
}

interface TestRepositoryContract
{
}

class TestRepository extends BaseRepository implements TestRepositoryContract
{
    public function __construct(TestModel $model)
    {
        parent::__construct($model);
    }
}

class TestModel extends BaseModel
{
    protected $table = 'test_models';
    protected $fillable = ['name'];
}

// Relation Testing Helpers

class TestEntityOneToOne extends TestEntity
{
    use EntityHasRelations;

    public function getOwned()
    {
        return $this->hasOne(TestEntityOneToOneInverse::class, 'relation_id');
    }
}

class TestEntityOneToOneInverse extends TestEntity
{
    use EntityHasRelations;

    public function getOwner()
    {
        return $this->belongsTo(TestEntityOneToOne::class, 'relation_id');
    }
}

class TestEntityPolymorphicOneToOne extends TestEntity
{
    use EntityHasRelations;

    public function getOwner()
    {
        return $this->morphOne(TestEntityPolymorphicOneToOneInverse::class, 'relation_type', 'relation_id');
    }
}

class TestEntityPolymorphicOneToOneInverse extends TestEntity
{
    use EntityHasRelations;

    public function getOwned()
    {
        return $this->morphTo('relation_type', 'relation_id');
    }
}

class TestEntityOneToMany extends TestEntity
{
    use EntityHasRelations;

    public function getOwned()
    {
        return $this->hasMany(TestEntityOneToManyInverse::class, 'relation_id');
    }
}

class TestEntityOneToManyInverse extends TestEntity
{
    use EntityHasRelations;

    public function getOwner()
    {
        return $this->belongsTo(TestEntityOneToMany::class, 'relation_id');
    }
}


class TestEntityPolymorphicOneToMany extends TestEntity
{
    use EntityHasRelations;

    public function getOwned()
    {
        return $this->morphMany(TestEntityPolymorphicOneToManyInverse::class, 'relation_type', 'relation_id');
    }
}

class TestEntityPolymorphicOneToManyInverse extends TestEntity
{
    use EntityHasRelations;

    public function getOwner()
    {
        return $this->morphTo('relation_type', 'relation_id');
    }
}

class TestEntityManyToMany extends TestEntity
{
    use EntityHasRelations;

    protected $table = 'testA';

    public function getOwners()
    {
        return $this->belongsToMany(TestEntityManyToManyInverse::class);
    }

}

class TestEntityManyToManyInverse extends TestEntity
{
    use EntityHasRelations;

    protected $table = 'testB';

    public function getOwned()
    {
        return $this->belongsToMany(TestEntityManyToMany::class);
    }
}


class TestEntityPolymorphicManyToMany extends TestEntity
{
    protected $table = 'testA';

    use EntityHasRelations;

    public function getOwners()
    {
        return $this->morphToMany(TestEntityPolymorphicManyToManyInverse::class, 'relatables');

        $pivots = DB::select('select * from testA_to_testB where testA_id = (?)', [$this->getId()]);

        $owners = array_map(function($pivot) {
            return app()->make($pivot->testB_type)->getById($pivot->testB_id);
        }, $pivots);

        return $owners;
    }
}

class TestEntityPolymorphicManyToManyInverse extends TestEntity
{
    protected $table = 'testB';
    use EntityHasRelations;

    public function attachOwned($entity)
    {
        DB::insert('insert into relatables (testB_id, testA_id, testB_type, testA_type) values (?, ?, ?, ?)', [$this->getId(), $entity->getId(), $this->getType(), $entity->getType()]);
    }

    public function getOwned()
    {
        return $this->morphedByMany('relatables');

        $pivots = DB::select('select * from testA_to_testB where testB_id = (?)', [$this->getId()]);

        $owners = array_map(function($pivot) {
            return app()->make($pivot->testA_type)->getById($pivot->testA_id);
        }, $pivots);

        return $owners;
    }
}
