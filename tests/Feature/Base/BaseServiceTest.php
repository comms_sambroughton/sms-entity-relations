<?php

namespace Tests\Feature;

use App\Entities\BaseEntity;
use App\Exceptions\RestrictedEntity;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Tests\TestRepository;
use Tests\TestRepositoryContract;
use Tests\TestService;

class BaseServiceTest extends TestCase
{
    protected $testService;

    protected function setUp()
    {
        parent::setUp();
        $this->makeTestTables('test_models');
        $this->testService = app()->make(TestService::class);
    }

    /** @test */
    function it_can_create_an_entity()
    {
        $this->testService->create(['name' => 'Test Model']);

        $model = $this->testService->getLast();

        $this->assertDatabaseHas('test_models', ['name' => 'Test Model']);
        $this->assertInstanceOf(BaseEntity::class, $model);
        $this->assertEquals('Test Model', $model->getName());
    }

    /** @test */
    function creating_an_entity_returns_an_immutable_entity()
    {
        $entity = $this->testService->create(['name' => 'Test Model']);

        $this->expectException(RestrictedEntity::class);
        $this->expectExceptionMessage('Action not permitted. Please use a service to perform this action.');

        $entity->create([]);
    }

    /** @test */
    function it_can_update_an_entity()
    {
        $entity = $this->testService->create(['name' => 'Test Model']);

        $this->testService->update($entity->getId(), ['name' => 'Updated Model']);

        $this->assertDatabaseHas('test_models', ['name' => 'Updated Model']);
        $this->assertCount(1, $this->testService->getAll());
        $this->assertEquals('Updated Model', $this->testService->getLast()->getName());
    }

    /** @test */
    function it_can_delete_an_entity()
    {
        $entityId = $this->testService->create(['name' => 'Test Model'])->getId();

        $this->testService->delete($entityId);

        $this->assertDatabaseMissing('test_models', ['name' => 'Test Model']);
        $this->assertCount(0, $this->testService->getAll());
    }
}
