<?php

namespace App\Exceptions;

use Exception;

class RestrictedEntity extends Exception
{
    public static function actionNotPermitted()
    {
        return new static("Action not permitted. Please use a service to perform this action.");
    }
}