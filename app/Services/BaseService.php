<?php

namespace App\Services;

use App\Entities\BaseEntity;

abstract class BaseService
{
    /**
     * @var BaseEntity
     */
    private $entity;

    public function __construct(BaseEntity $entity)
    {
        $this->entity = $entity;
    }

    public function create($data): BaseEntity
    {
        return $this->entity->create($data);
    }

    public function update($id, array $data): bool
    {
        return $this->entity->update($id, $data);
    }

    public function delete($id): bool
    {
        return $this->entity->delete($id);
    }

    public function getAll()
    {
        return $this->entity->getAll();
    }

    public function getLast(): BaseEntity
    {
        return $this->entity->getLast();
    }
}