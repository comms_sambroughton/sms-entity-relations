<?php

namespace App\Entities;

use App\Contracts\BaseRepositoryContract;
use App\Exceptions\RestrictedEntity;
use App\Traits\EntityHasRelations;

abstract class BaseEntity
{
    use EntityHasRelations;

    protected $id;
    protected $table;
    protected $exists;
    /**
     * @var BaseRepositoryContract
     */
    protected $repo;

    public function __construct(BaseRepositoryContract $repo)
    {
        $this->repo = $repo;
    }

    public function create($data)
    {
        if ($this->exists) throw RestrictedEntity::actionNotPermitted();
        $model = $this->repo->create($data);
        return $this->buildEntity($model);
    }

    public function update($id, $data)
    {
        if ($this->exists) throw RestrictedEntity::actionNotPermitted();
        $this->repo->update($id, $data);
        return true;
    }

    public function delete($id)
    {
        if ($this->exists) throw RestrictedEntity::actionNotPermitted();
        $this->repo->delete($id);
        return true;
    }

    public function getLast()
    {
        $model = $this->repo->getLast();
        return $this->buildEntity($model);
    }

    public function getById($id)
    {
        $model = $this->repo->getById($id);
//        if (!$model) dd($id);
        return $this->buildEntity($model);
//        return $model ? $this->buildEntity($model) : null;
    }

    public function getAll()
    {
        $models = $this->repo->getAll();
        return $this->buildEntities($models);
    }

    public function getFirstWhere(array $condition)
    {
        $model = $this->repo->getFirstWhere($condition);
        return $model ? $this->buildEntity($model) : null;
    }

    public function getAllWhere(array $condition)
    {
        $models = $this->repo->getAllWhere($condition);
        return $this->buildEntities($models);
    }

    public function newInstance()
    {
        return new static($this->repo);
    }

    public function buildEntity($model)
    {
        $entity = $this->newInstance();
        $entity->setAttributes($model->toArray());
        $entity->exists = true;
        return $entity;
    }

    public function buildEntities($models)
    {
        return $models->map(function ($model) {
            return $this->buildEntity($model);
        });
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return static::class;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->repo->getTableName();
    }
}