<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait EntityHasRelations
{
    public function hasOne($class, $foreignKey)
    {
        return app()->make($class)->getFirstWhere([$foreignKey => $this->getId()]);
    }

    public function belongsTo($class, $foreignKey)
    {
        return app()->make($class)->getById($this->$foreignKey);
    }

    public function morphOne($class, $typeKey, $idKey)
    {
        return app()->make($class)->getFirstWhere([$typeKey => $this->getType(), $idKey => $this->getId()]);
    }

    public function morphTo($typeKey, $idKey)
    {
        return app()->make($this->$typeKey)->getById($this->$idKey);
    }

    public function hasMany($class, $foreignKey)
    {
        return app()->make($class)->getAllWhere([$foreignKey => $this->getId()]);
    }

    public function morphMany($class, $typeKey, $idKey)
    {
        return app()->make($class)->getAllWhere([$typeKey => $this->getType(), $idKey => $this->getId()]);
    }

    public function belongsToMany($class)
    {
        list($table, $relatedTable, $pivotTable) = $this->getTableNames(app()->make($class));

        $pivots = $this->repo->getBelongsToMany($this->getId(), $table, $pivotTable);

        $relations = array_map(function ($pivot) use ($relatedTable, $class) {
            return app()->make($class)->getById($pivot->{$relatedTable . '_id'});
        }, $pivots);

        return collect($relations);
    }

    public function morphToMany($class, $relation)
    {
        $relationTable = str_plural($relation);

        $pivots = $this->repo->getMorphToMany($this->getId(), $this->getType(), $relation, $relationTable);

        $relations = array_map(function ($pivot) use ($class) {
            return app()->make($class)->getById($pivot->{'baz_id'});
        }, $pivots);

        return collect($relations);
    }

    public function morphedByMany($class, $relation)
    {
        $name = str_singular($this->getTableName());
        $relationTable = str_plural($relation);

        $pivots = $this->repo->getMorphedByMany($this->getId(), $class, $name, $relation, $relationTable);

        $relations = array_map(function ($pivot) use ($class, $relation) {
            return app()->make($class)->getById($pivot->{$relation . '_id'});
        }, $pivots);

        return collect($relations);
    }


    public function attachBelongsToMany($relatedEntity)
    {
        list($table, $relatedTable, $pivotTable) = $this->getTableNames($relatedEntity);

        $this->repo->attachBelongsToMany($this->getId(), $table, $relatedEntity->getId(), $relatedTable, $pivotTable);
    }

    public function attachMorphToMany($relation, $entity)
    {
        $entityName = str_singular($entity->getTableName());
        $relationTable = str_plural($relation);

        $this->repo->attachMorphToMany($this->getId(), $this->getType(), $entity->getId(), $entityName, $relation, $relationTable);
    }

    public function attachMorphedByMany($relation, $entity)
    {
        $name = str_singular($this->getTableName());
        $relationTable = str_plural($relation);

        $this->repo->attachMorphedByMany($this->getId(), $name, $entity->getId(), $entity->getType(), $relation, $relationTable);
    }

    public function getTableNames($relatedEntity)
    {
        $table = str_singular($this->getTableName());
        $relatedTable = str_singular($relatedEntity->getTableName());
        $pivotTable = collect([$table, $relatedTable])->sort()->implode('_');

        return [$table, $relatedTable, $pivotTable];
    }

}