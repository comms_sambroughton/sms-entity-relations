<?php

namespace App\Repositories;

use App\Contracts\BaseRepositoryContract;
use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

abstract class BaseRepository implements BaseRepositoryContract
{
    /**
     * @var BaseModel
     */
    private $model;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data)
    {
        $this->model->find($id)->update($data);
    }

    public function delete($id)
    {
        $this->model->find($id)->delete();
    }

    public function getLast()
    {
        return $this->model->all()->last();
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getFirstWhere(array $condition)
    {
        return $this->model->where($condition)->first();
    }

    public function getAllWhere(array $condition)
    {
        return $this->model->where($condition)->get();
    }

    public function getTableName()
    {
        return $this->model->getTableName();
    }

    public function attachBelongsToMany($id, $table, $relatedId, $relatedTable, $pivotTable)
    {
        DB::insert("insert into {$pivotTable} ({$table}_id, {$relatedTable}_id) values (?, ?)", [$id, $relatedId]);
    }

    public function getBelongsToMany($id, $name, $pivotTable )
    {
        return DB::select("select * from {$pivotTable} where {$name}_id = (?)", [$id]);
    }

    public function attachMorphToMany($id, $type, $entityId, $entityName, $relation, $relationTable)
    {
        DB::insert("insert into {$relationTable} ({$entityName}_id, {$relation}_id, {$relation}_type) values (?, ?, ?)",
            [$entityId, $id, $type]
        );
    }

    public function getMorphToMany($id, $type, $name, $relationTable)
    {
        return DB::select("select * from {$relationTable} where {$name}_id = (?) and {$name}_type = (?)", [$id, $type]);
    }

    public function attachMorphedByMany($id, $name, $entityId, $entityType, $relation, $relationTable)
    {
        DB::insert("insert into {$relationTable} ({$name}_id, {$relation}_id, {$relation}_type) values (?, ?, ?)",
            [$id, $entityId, $entityType]
        );
    }

    public function getMorphedByMany($id, $type, $name, $relation, $relationTable)
    {
        return DB::select("select * from {$relationTable} where {$name}_id = (?) and {$relation}_type = (?)", [$id, $type]);
    }
}